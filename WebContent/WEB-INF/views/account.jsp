
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ page session="false" %>
<html>
<head>
	<title>Account Page</title>
	<style type="text/css">
		.tg  {border-collapse:collapse;border-spacing:0;border-color:#ccc;}
		.tg td{font-family:Arial, sans-serif;font-size:14px;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;border-color:#ccc;color:#333;background-color:#fff;}
		.tg th{font-family:Arial, sans-serif;font-size:14px;font-weight:normal;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;border-color:#ccc;color:#333;background-color:#f0f0f0;}
		.tg .tg-4eph{background-color:#f9f9f9}
	</style>
</head>
<body>
<h1>
	Add a account
</h1>

<c:url var="addAction" value="/account/add" ></c:url>

<form:form action="${addAction}" commandName="account">
<table>
	<c:if test="${!empty account.email}">
	<tr>
		<td>
			<form:label path="id">
				<spring:message text="ID"/>
			</form:label>
		</td>
		<td>
			<form:input path="id" readonly="true" size="8"  disabled="true" />
			<form:hidden path="id" />
		</td> 
	</tr>
	</c:if>
	<tr>
		<td>
			<form:label path="email">
				<spring:message text="Email"/>
			</form:label>
		</td>
		<td>
			<form:input path="email" />
		</td> 
	</tr>
	<tr>
		<td>
			<form:label path="nickname">
				<spring:message text="Nickname"/>
			</form:label>
		</td>
		<td>
			<form:input path="nickname" />
		</td>
	</tr>
	<tr>
		<td colspan="2">
			<c:if test="${!empty account.email}">
				<input type="submit"
					value="<spring:message text="Edit account"/>" />
			</c:if>
			<c:if test="${empty account.email}">
				<input type="submit"
					value="<spring:message text="Add account"/>" />
			</c:if>
		</td>
	</tr>
</table>	
</form:form>
<br>
<h3>accounts List</h3>
<c:if test="${!empty listAccounts}">
	<table class="tg">
	<tr>
		<th width="80">account ID</th>
		<th width="120">account Name</th>
		<th width="120">account Country</th>
		<th width="60">Edit</th>
		<th width="60">Delete</th>
	</tr>
	<c:forEach items="${listAccounts}" var="account">
		<tr>
			<td>${account.id}</td>
			<td>${account.email}</td>
			<td>${account.nickname}</td>
			<td><a href="<c:url value='/edit/${account.id}' />" >Edit</a></td>
			<td><a href="<c:url value='/remove/${account.id}' />" >Delete</a></td>
		</tr>
	</c:forEach>
	</table>
</c:if>
</body>
</html>