Analisi dei casi d'uso possibili per il DM con Unity
===================================================

Caso 1 (Minimal Unity Usage)
----------------------------

Il dungeon master dal sito crea una lobby selezionando storia e ricompense da quelli acquistati sul sito, viene aperta l'applicazione unity, con le credenziali di accesso e l'impostazione della partita passate dal sito al gioco. Al DM non resta che aprire una lobby e invitare i giocatori.
A partita conclusa il DM viene reindirizzato fuori da ~~webplayer~~ Unity. (Il webplayer Unity non è più supportato.)

**In questo caso d'uso la partita viene creata al momento dell'apertura di Unity e vanno pensati bene i parametri per la creazione a priori.** *Richiede che l'applicazione venga riavviata per ogni partita*.

Caso 2 (Unity-Centered Game)
---------------------------

Il dungeon master ed i giocatori avviano l'applicazione Unity, dove effettueranno l'accesso con le credenziali del sito, nell'applicazione il DM ha a disposizione l'inventario (*gli oggetti comprati/creati sul sito*); da qui può creare una lobby ed assegnare alla lobby una storia e delle ricompense, invitare i giocatori, cambiare alcune impostazioni di gioco.
A partita conclusa l'applicativo resta in esecuzione e il DM decide se giocare di nuovo e con quali impostazioni(storia e ricompense).

**In questo caso d'uso la partita è un livello dentro Unity, il DM avrà possibilità di visualizzare in anteprima *almeno* l'ambientazione di gioco al momento di selezione della storia.**

