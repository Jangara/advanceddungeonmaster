**AdvancedDungeonMaster**
====================
Requisiti versione 1.0.1
--------------------------

Sviluppatori: Emanuele Acquista,  Giuseppe Carrella,  Luca ~~Proplayer~~ Bandieramonte

Importante:
---
**Questo file è da considerarsi manifesto del progetto, i contribuenti al progetto faranno riferimento a questo file per continuare il lavoro, pertanto è importante tenerlo aggiornato. Per modificare questo ReadMe in markdown (semplice linguaggio di formattazione) seguire le linee guida [qui](https://github.com/adam-p/markdown-here/wiki/Markdown-Cheatsheet).**

Obiettivi:
--------- 
___
L'utente registrato ha la possibilità di creare e vendere le proprie storie sul market della piattaforma
L'utente ha la possibilità di giocare, insieme ad un party, con i libri acquistati seguendo un sistema di gioco comune a tutta la piattaforma

  + ### Sezioni della piattaforma:
  1. Editor Storie
  1. La mia sacca conservante
  1. Market
  1. Arena dove l'utente consuma le sue storie
  1. Aiutaci con suggerimenti 

Sezione 1 – Editor Storie
---------------------
____

In questa sezione l'utente crea la sua storia seguendo regole comuni. Il dominio delle regole è così formato:

+ Titolo [no bestemmie e parolacce]
+ Autore [NickName utente]
+ Introduzione [++Preview per un utente che vuole acquistare una storia]
+ Linee guida per la creazione  della scheda pg [pf = lancia d20 + moltiplicatore definito dalla piattaforma]
+ Popolamento della tabella dei mostri con definizione parametri
+ Popolamento della tabella NPG
+ Struttura in paragrafi
+ Associazioni dadi a eventi

In quest'area l'utente può inoltre salvare la storia e continuarla in un secondo momento, oppure caricare sulla piattaforma la storia appena creata. La storia sarà visibile nella bottega delle storie insieme alla data di pubblicazione, alle visualizzazioni anteprima dell'introduzione, al numero di acquisti, al rating ottenuto (boccali di birra), commenti ricevuti.

Sezione 2 – La mia sacca conservante
-----------------------------------
____

Nella sezione “la mia sacca conservante” sono contenute tutte le informazioni dell'avventuriero:

+ Nome
+ Cognome
+ NickName
+ Età
+ Riferimenti esterni (profili social)
+ Le mie storie (Storie scritte dall'avventuriero)
+ Le storie acquistate (Storie acquistate dall'avventuriero)
+ Personaggio - Qui l'utente può creare e gestire il proprio personaggio, creare un background del personaggio, assegnargli una classe (guerriero, mago, ladro... etc) ed assegnare punti alle caratteristiche (Alla creazione del personaggio verranno resi disponibili 10 punti, ogni livello ulteriori 5 punti. Livello massimo personaggio 30. Punti massimi per caratteristica: 100.):
   + Forza
   + Costituzione
   + Agilità
   + Intelligenza
   + Saggezza
   + Fortuna
+ Oggetti acquisiti durante le arene (Oro, Armi, Consumabili)
+ Sezione Messaggi
+ Sezione Notifiche
+ Crea Storia (Qui l'utente viene rimandato all'editor di creazione delle storie)
+ Le mie pergamene (i miei messaggi) versione 2.0
In questa sezione l'utente può scambiare messaggi con gli autori delle storie per chiedere consiglio o per dare un feedback personale.
+ I miei corvi (le mie notifiche)
L'utente riceve notifiche quando:
  + un altro utente acquista una sua storia
  + un altro utente dà una valutazione ad una sua storia
  + la sua storia viene segnalata



Sezione 3 - La Taverna [Market] (segnala abuso ecc)
--------------------------------
____

Nella taverna gli utenti possono visualizzare i libri game pubblicati dagli autori, visualizzare l’anteprima del libro (costituita dall’introduzione della stessa) ed eventualmente comprarlo.
Il market ha due sezioni così costituite:

+ Libri
+ Più venduti
+ In evidenza
+ Nuovi
+ Autori
+ I più votati
 
Nella lista dei libri è possibile visualizzare accanto ad ogni nome: il numero di commenti rilasciati ed il relativo rating.

### Proprietà Libro:
+ Titolo
+ Autore
+ Prezzo
+ Rating
+ Commenti
+ Foto di Copertina
+ Tasto Visualizza Anteprima [Introduzione della Storia]
+ Difficoltà [Versione 2.0]
+ Numero di Vendite
+ Data di Rilascio

### Proprietà Autore:
+ Nickname
+ Rating
+ Funzionalità:
  + Cerca
Tramite questa funzionalità è possibile inserire il nome di un libro o di un autore. Il sistema effettuerà una ricerca in base all’input e restituirà una lista dei risultati ottenuti.
  + Metti nel Carrello
Tramite questa funzionalità è possibile inserire nel carrello il libro game selezionato. I libri game nel carrello potranno essere acquistati solamente  se al profilo dell’utente è associato un metodo di pagamento.
I libri game nel carrello potranno essere acquistati solo se l’utente indica un metodo di pagamento valido all’atto dell’acquisto
  + Vedi Anteprima
Tramite questa funzionalità è possibile visualizzare l’introduzione del libro game selezionato.
  + Visualizza Autore
Tramite questa funzionalità è possibile visualizzare il profilo dell’autore del librogame selezionato.

Sezione 4 - Arena 
-----------------
____

## Questa sezione al momento è in fase di studio.

Nell’Arena è possibile giocare ai propri libri game o a quelli acquistati.
Il tipo di gioco è da scegliere: 2D side scroller, 2D roguelike/dungeoncrawler o LowPoly3D ortographic.
Durata di una partita da definire.

Ciascun giocatore potrà giocare il proprio personaggio creato nella sacca conservante.
Dalla creazione dell'avatar verranno assegnati HP e MP a seconda della classe selezionata e dei punti caratteristica assegnati. Inoltre verranno prelevati dalla sacca conservante le informazioni sugli oggetti ottenuti nelle precedenti arene.

L'arena sarà creata proceduralmente basandosi su alcuni tag ricavati dalla storia di riferimento.
Saranno presenti diverse ambientazioni da matchare il più possibile fedelmente con quella della storia.

Il DungeonMaster oltre alla scelta della storia potrà posizionare in tempo reale alcuni NPC nella mappa di gioco. (Max numero predeterminato di NPC)

Saranno presenti 2 tipologie di NPC:
+ Aggressivi con cui ingaggiare combattimenti
+ NonAggressivi che faranno partire un evento caratteristica (Gli eventi metteranno alla prova una (o più) caratteristiche del personaggio)

Evento caratteristica:
+ Negli eventi si tirano dadi insieme all'NPC.
+ Vengono applicati moltiplicatori per la caratteristica dell'evento al lancio del dado (Evento Saggezza: Giocatore ha 10 punti saggezza, fa 6 col dado viene aggiunto 10%, punteggio del lancio 6,6.)
+ Il primo a raggiungere un numero stabilito (50) col lancio dei dadi vince l'evento
+ In caso di vincita il giocatore ottiene 1 punto caratteristica da assegnare nella sacca conservante.
+ In caso di perdita il giocatore perde 1 punto caratteristica e il 5-10% degli HP.

Combattimenti:
+ Combattimenti a turni
+ Le azioni dell’avversario sono Attacco, Incantesimo e Schiva
+ L’azione dell’avversario viene scelta random.
+ Se gli MP dell’avversario sono minori di quelli necessari per effettuare l’incantesimo, viene ripetuto il tiro per la scelta dell’azione.

La partita si conclude quando i giocatori battono tutti gli NPC presenti, oppure quando tutti i giocatori muoiono.

Funzionalità:
+ Lanciare dadi
+ Prendere decisioni
+ Combattere
+ Chiudere il libro con salvataggio automatico
+ Cancellare i progressi di una storia giocata
+ Votare e commentare il libro una volta concluso
+ Caricare una partita

Sezione 5 - Suggerimenti
-------------------------
___
Nell’area dei Suggerimenti è possibile inviare una mail agli sviluppatori con dei suggerimenti [Capitan Ovvio]

BOT per chattare con un operatore fittizio per inviare i suggerimenti  versione 3.0

# TECNOLOGIE

+ Entando (Java, Spring, Struts) [ApplicationServer: Jetty] * superato *
+ GameSpark
+ Spring (Cloud, Security, Eureka, Microservices)
+ Kafka * per gli stream dei dati *
+ JXTA * Pacchetti e gestione pacchetti *
+ Cenni all’uso di Maven (basilari)
+ PostgreSQL e MySQL
+ CSS [Less/Sass]
+ HTML
+ JSP
+ Javascript
+ JQuery
+ Bootstrap [v4.0]
+ Unity 

Link utili: 

[Entando Get Started](https://github.com/entando/entando-core/wiki/Getting-Started) 

[Entando Build From Source](https://github.com/entando/entando-core/wiki/Build-from-source-code)

[Entando How The Core Works](https://github.com/entando/entando-core/wiki/How-the-Core-works)

[Entando How To Use The Core](https://github.com/entando/entando-core/wiki/How-to-use-the-Core)

[Unreal To Unity](https://docs.unrealengine.com/en-us/GettingStarted/FromUnity) *in verità è Unity to Unreal*

[Unity NetworkingManager](https://docs.unity3d.com/ScriptReference/Networking.NetworkManager.html)

![alt text](https://m.media-amazon.com/images/S/aplus-media/sota/1c4d6642-dd6f-44b9-9900-bff32eb889ad.jpg "Peppegay")


