package com.adm.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "accounts")
public class Account implements Serializable {
	
	private static final long serialVersionUID = -3491070371840773219L;

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Long id;
	
	 @Column(name = "email")
	 private String email;
	 
	 @Column(name = "nickname")
	 private String nickname;
	 
	 
	 public Long getId() {
		return id;
	}


	public void setId(Long id) {
		this.id = id;
	}


	public String getEmail() {
		return email;
	}


	public void setEmail(String email) {
		this.email = email;
	}


	public String getNickname() {
		return nickname;
	}


	public void setNickname(String nickname) {
		this.nickname = nickname;
	}


	@Override
	 public String toString() {
		 return "\nEMAIL: " + this.getEmail() + 
				 "\nNICKNAME: " + this.getNickname();
	 }
}
