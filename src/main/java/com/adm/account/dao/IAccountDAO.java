package com.adm.account.dao;

import java.util.List;

import com.adm.entity.Account;

public interface IAccountDAO {
	
	public void addAccount(Account account);
	public List<Account> listAccounts();
}
