package com.adm.account.dao;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.adm.entity.Account;

@Repository
public class AccountDAO implements IAccountDAO {
	
	private static final Logger logger = LoggerFactory.getLogger(AccountDAO.class);
	
	@Autowired
	private SessionFactory sessionFactory;
	
	public void setSessionFactory(SessionFactory sf) {
		this.sessionFactory = sf;
	}
	
	@Override
	public void addAccount(Account account) {
		Session session = this.sessionFactory.getCurrentSession();
		session.persist(account);
		logger.info("Account salvato correttamente... " + account.toString());
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<Account> listAccounts(){
		return sessionFactory.getCurrentSession().createQuery("from Account").list();
	}
}
