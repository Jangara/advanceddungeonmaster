package com.adm.account.service;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.adm.account.dao.AccountDAO;
import com.adm.account.dao.IAccountDAO;
import com.adm.entity.Account;


@Service
@Transactional
public class AccountService implements IAccountService{
	
	private static final Logger logger = LoggerFactory.getLogger(AccountService.class);
	
	@Autowired
	private IAccountDAO accountDAO;
	
	public void setAccountDAO(IAccountDAO accountDAO) {
		this.accountDAO = accountDAO;
	}
	
	@Override 
	@Transactional
	public void addAccount(Account account) {
		this.accountDAO.addAccount(account);
	}
	
	@Override
	@Transactional
	public List<Account> listAccounts(){
		List<Account> accounts = new ArrayList<Account>();
		accounts = this.accountDAO.listAccounts();
		for(Account a : accounts) {
			logger.info(a.toString());
		}
		return accounts;
	}
}
