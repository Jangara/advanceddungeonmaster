package com.adm.account.service;

import java.util.List;

import com.adm.entity.Account;

public interface IAccountService {
	
	public void addAccount(Account account);
	public List<Account> listAccounts();
}
