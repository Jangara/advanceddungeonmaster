package com.adm.account.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.adm.account.service.IAccountService;
import com.adm.entity.Account;

@Controller
public class AccountController {
	
	@Autowired
	private IAccountService accountService;
	
	/*
	@Autowired(required = true)
	@Qualifier(value = "accountService")
	public void sertAccountService(IAccountService accountService) {
		this.accountService = accountService;
	}
	*/
	
	@RequestMapping(value = "/accounts", method = RequestMethod.GET)
	public String listAccounts(Model model) {
		
		model.addAttribute("account", new Account());
		model.addAttribute("listAccounts", this.accountService.listAccounts());
		return "account";
		
	}
	
	@RequestMapping(value = "/account/add", method = RequestMethod.POST)
	public String addAccount(@ModelAttribute("account") Account account) {
		
		if(account.getId() == 0) {
			/* L'account non esiste e viene aggiunto */
			this.accountService.addAccount(account);
		}else {
			/* L'account esiste e allora viene aggiornato */
			//TODO : implementare update account 
		}
		
		return "redirect:/accounts";
	}
}
